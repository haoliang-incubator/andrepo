#!/usr/bin/env bash

root=$(dirname "$(realpath "$0")")

cd "$root" || exit 1

while true; do
    read -rp "pkg: " pkg 
    [ -z "$pkg" ] && continue
    adb shell pm disable-user --user 0 "$pkg"
    echo "$pkg" >> disabled.log
done
