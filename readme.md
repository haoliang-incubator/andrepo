ref:
* https://developer.android.com/studio/command-line/adb


hardware info:
* cpuinfo
* architecture: `uname -m`

software info:
* adb shell getprop ro.build.version.release
* adb shell getprop ro.build.version.sdk
* adb shell getprop

useful programs in android:
* am (activity manager)
* pm (package manager)
* getprop


known:
* VNX9X19629002568 | aarch64
* T1011S64GBCN20402580604 | aarch64 | 10
* 4e4927c8 | aarch64


shared package list:
* vivaldi
* totalcommand
* totalcmd-wifi
* vlc
* password store
* open keychain
* igniter
* foobar


apk sources:
* apkmirror
