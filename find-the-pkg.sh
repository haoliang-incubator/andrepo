#!/usr/bin/env bash

set -e

root=$(dirname "$(realpath "$0")")

cd "$root"

>&2 echo "saving ps facts: a"
adb shell ps | awk '{ print $NF }' | sort > ps.a

>&2 echo "close the process on the android device side"
read -r -p "done and continue?"

>&2 echo "saving ps facts: b"
adb shell ps | awk '{ print $NF }' | sort > ps.b

diff ps.a ps.b
