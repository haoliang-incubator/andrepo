from . import connection


class HostService:
    def __init__(self, host, port):
        self._round_trip = connection.host_round_trip(host, port)

    async def version(self):
        return await self._round_trip("host:version")

    async def kill(self):
        return await self._round_trip("host:kill")

    async def devices(self):
        return await self._round_trip("host:devices")


class LocalService:
    def __init__(self, host, port, serial_number):
        self._round_trip = connection.transport_round_trip(host, port, serial_number)

    async def getprop(self):
        return await self._round_trip("getprop")

    async def framebuffer(self):
        raise NotImplementedError()
