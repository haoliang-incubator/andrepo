def pack(data: str) -> bytes:
    encoded = data.encode()
    header = format(len(encoded), "0>4X").encode()

    return header + encoded


OKAY = b"OKAY"
FAIL = b"FAIL"
