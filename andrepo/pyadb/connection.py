import select
import struct
import typing as T

import trio

from . import protocol

BUFSIZE = select.PIPE_BUF


async def receive_exactly(sock: trio.SocketStream, length: int) -> T.List[bytes]:
    remain = length
    chunks = []

    while remain > 0:
        chunk = await sock.receive_some(BUFSIZE)
        if chunk == b"":
            raise BrokenPipeError("remote closed the connection")
        remain -= len(chunk)
        chunks.append(chunk)

    return chunks


async def receive_all_with_fixed_length(sock: trio.SocketStream) -> T.List[bytes]:

    first = await sock.receive_some(BUFSIZE)

    if first == b"":
        raise BrokenPipeError("remote closed the connection")

    # {'OKAY','FAIL'} + length
    remain = int(first[4:8], 16) + 8 - len(first)

    if remain == 0:
        return first

    chunks = await receive_exactly(sock, remain)
    chunks.insert(0, first)

    return chunks


async def receive_all_until_empty(sock: trio.SocketStream):
    chunks = []
    while True:
        chunk = await sock.receive_some()
        if chunk == b"":
            break
        chunks.append(chunk)

    return chunks


async def ok_round_trip(sock: trio.SocketStream, request: str):
    await sock.send_all(protocol.pack(request))

    # should only receive once
    output = await sock.receive_some(BUFSIZE)
    if output == b"":
        raise ConnectionError("remote closed the connection")

    if not output.startswith(protocol.OKAY):
        raise ConnectionError(f"failed to request: {request}")

    return output


def host_round_trip(host: str, port: int):
    async def round_trip(request: str, timeout: int = 10):
        with trio.fail_after(timeout):
            async with await trio.open_tcp_stream(host, port) as sock:
                await sock.send_all(protocol.pack(request))

                return await receive_all_with_fixed_length(sock)

    return round_trip


def transport_round_trip(host: str, port: int, serial_number: str):
    async def round_trip(request: str, timeout: int = 10):
        with trio.fail_after(timeout):
            sock: trio.SocketStream
            async with await trio.open_tcp_stream(host, port) as sock:
                await ok_round_trip(sock, f"host:transport:{serial_number}")
                await ok_round_trip(sock, request)

                return await receive_all_until_empty(sock)

    return round_trip


def framebuff_round_trip(host: str, port: int, serial_number: str):
    async def round_trip(timeout: int = 10):
        with trio.fail_after(timeout):
            sock: trio.SocketStream
            async with await trio.open_tcp_stream(host, port) as sock:
                await ok_round_trip(sock, f"host:transport:{serial_number}")
                await ok_round_trip(sock, "framebuffer:")

                return await receive_all_until_empty(sock)

    return round_trip
