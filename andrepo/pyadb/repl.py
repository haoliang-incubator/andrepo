import shlex

import trio
from andrepo.pyadb import connection


async def screenshot():
    round_trip = connection.framebuff_round_trip(
        "127.0.0.1", 5037, "T1011S64GBCN20402580604"
    )

    fb_chunks = await round_trip()
    binfile = trio.Path("/tmp/screenshot.bin")
    imgfile = trio.Path("/tmp/screenshot.png")

    async with await binfile.open("wb") as fp:
        for chunk in fb_chunks:
            await fp.write(chunk)

    await imgfile.unlink(missing_ok=True)

    await trio.run_process(
        shlex.split(
            "ffmpeg -loglevel panic -f rawvideo -pix_fmt bgr32 -s {resolution} -i {infile} -vcodec png -vframes 1 {outfile}".format(
                resolution="1920x1200", infile=binfile, outfile=imgfile
            )
        ),
        check=True,
    )


async def main():
    # hostsvc = client.HostService("127.0.0.1", 5037)
    # async def host_version(id):
    #     print(id, await hostsvc.version())
    # async with trio.open_nursery() as nursery:
    #     for id in range(5):
    #         nursery.start_soon(host_version, id)
    # localsvc = client.LocalService("127.0.0.1", 5037, "T1011S64GBCN20402580604")
    # print(await localsvc.getprop())
    await screenshot()


if __name__ == "__main__":
    trio.run(main)
