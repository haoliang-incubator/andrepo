import select

import trio
from andrepo.pyadb import protocol


async def host_version():
    stream: trio.SocketStream = await trio.open_tcp_stream("127.0.0.1", 5037)
    async with stream:
        await stream.send_all(protocol.pack("host:version"))
        out = await stream.receive_some(select.PIPE_BUF)

        if out == b"":
            raise SystemExit("remote closed the connection")

        assert len(out) >= 4

        print(out)


async def shell(command: str):
    stream: trio.SocketStream = await trio.open_tcp_stream("127.0.0.1", 5037)
    async with stream:
        await stream.send_all(protocol.pack("host:transport:T1011S64GBCN20402580604"))
        out = await stream.receive_some(select.PIPE_BUF)

        if out == b"":
            raise SystemExit("remote closed the connection")

        if out != b"OKAY":
            raise RuntimeError("failed to create proxy")

        await stream.send_all(protocol.pack("shell:{}".format(command)))

        out = await stream.receive_some(select.PIPE_BUF)

        if out == b"":
            raise SystemExit("remote closed the connection")

        if out != b"OKAY":
            raise RuntimeError("failed to issue service")

        # NOTE: could lose some data
        out = await stream.receive_some(select.PIPE_BUF)

        if out == b"":
            raise SystemExit("remote closed the connection")

        print(out)


async def main():
    # await host_version()
    await shell("echo hello")


if __name__ == "__main__":
    trio.run(main)
